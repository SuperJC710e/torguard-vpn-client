#!/bin/sh
# Needs work
set -x
_newver=${1:-4.4.2}
_oldver=$(sed -n 's/^[ \t]\+<Version>\([0-9]\+\.[0-9]\+\.[0-9]\+\)<\/Version>/\1/p' pspec.xml)
_outdir=/var/cache/eopkg/archives
_pkgname=torguard-latest-amd64.deb
_urlbase='https://torguard.net/downloads'
_date=$(date +%d-%m-%Y)
_oldrel=$(awk -F'"' '/release=/{gsub(/">/,"");print $2}' pspec.xml)
_newrel=$((_oldrel+1))
if [ ! -e ${_outdir}/${_pkgname} ];then
	sudo wget ${_urlbase}/${_pkgname} -P ${_outdir}
	if [ $? -ne 0 ];then
		echo -e "Download failed ... please investigate."; exit 2
	fi
else
	echo -e "Download file already exists"
fi
if [ -e ${_outdir}/${_pkgname} ];then
	_hash=$(sha1sum ${_outdir}/${_pkgname} | cut -f1 -d' ')
	if [ ! -z ${_hash} ];then echo -e "${_hash}";fi
else
	echo -e "File is missing, unable to hash ... please investigate."; exit 1
fi
sed -i -e "s/${_oldver}/${_newver}/g" -e "s/[0-9]{2}-[0-9]{2}-[0-9]{4}/${_date}/g" -e "s/release=\"${_oldrel}\"/release=\"${_newrel}\"/g" -e "s/\(sha1sum=\)\"[a-z0-9]*\"/\1\"${_hash}\"/g" pspec.xml
